# @nsi/file-type
```
Get file type from buffer or stream by signature checking
```
Libreria full javascript che esegue il parsing di un file (Buffer o Stream)
per il recupero del tipo esatto tramite l'analisi dei primi bytes.

Il confronto viene eseguito con le __firme__ indicate nel sito sottostante. 

## Firme / Signatures:
https://filesignatures.net

https://mark0.net/soft-trid-deflist.html

https://www.garykessler.net/library/file_sigs.html

## Usage / Utilizzo

```
const filtypeReader = require('@nsi/filtype-reader');

// TODO: DEMONSTRATE API
```

## How to collaborate
Ogni modifica alla libreria creare una nuova versione e aggiornare il
package.json.

minor changes, tests e fixes NON necessitano la creazione di una nuova versione.

Alla creazione di una nuova versione creare anche il tag corrispondente.