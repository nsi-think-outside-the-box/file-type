'use strict';

const fs = require('fs');
const path = require('path');
const assert = require('chai').assert;

const FiletypeReader = require('../lib/filetype-reader');

describe('@nsi/filtype-reader', () => {

  it('should return "application/pdf"', async () => {
    const dummyPdfStream = fs.createReadStream(path.join(__dirname, 'resources/dummy.pdf'));
    const reader = new FiletypeReader(dummyPdfStream);
    const fileInfo = await reader.read();
    assert.typeOf(fileInfo, 'object');
    assert.equal(fileInfo.ext, 'pdf');
    assert.equal(fileInfo.mime, 'application/pdf');
  });

  it('should return "application/zip"', async () => {
    const dummyZipStream = fs.createReadStream(path.join(__dirname, 'resources/dummy.zip'));
    const reader = new FiletypeReader(dummyZipStream);
    const fileInfo = await reader.read();
    assert.equal(fileInfo.ext, 'zip');
    assert.equal(fileInfo.mime, 'application/zip');
  });

  it('should return "message/rfc822"', async () => {
    const dummyZipStream = fs.createReadStream(path.join(__dirname, 'resources/email-message.eml'));
    const reader = new FiletypeReader(dummyZipStream);
    const fileInfo = await reader.read();
    assert.equal(fileInfo.ext, 'eml');
    assert.equal(fileInfo.mime, 'message/rfc822');
  });

  /// region Images
  it('should return "image/jpeg"', async () => {
    const stream = fs.createReadStream(path.join(__dirname, 'resources/image.jpg'));
    const reader = new FiletypeReader(stream);
    const fileInfo = await reader.read();
    assert.isArray(fileInfo.ext);
    assert.include(fileInfo.ext, 'jpeg');
    assert.include(fileInfo.ext, 'jpg');
    assert.equal(fileInfo.mime, 'image/jpeg');
  });

  it('should return "image/png"', async () => {
    const stream = fs.createReadStream(path.join(__dirname, 'resources/image.png'));
    const reader = new FiletypeReader(stream);
    const fileInfo = await reader.read();
    assert.equal(fileInfo.ext, 'png');
    assert.equal(fileInfo.mime, 'image/png');
  });

  it('should return "image/gif"', async () => {
    const stream = fs.createReadStream(path.join(__dirname, 'resources/image.gif'));
    const reader = new FiletypeReader(stream);
    const fileInfo = await reader.read();
    assert.equal(fileInfo.ext, 'gif');
    assert.equal(fileInfo.mime, 'image/gif');
  });
  /// endregion

  /// region Documents
  it('should return "application/vnd.openxmlformats-officedocument.wordprocessingml.document"', async () => {
    const stream = fs.createReadStream(path.join(__dirname, 'resources/document.docx'));
    const reader = new FiletypeReader(stream);
    const fileInfo = await reader.read();
    assert.equal(fileInfo.ext, 'docx');
    assert.equal(fileInfo.mime, 'application/vnd.openxmlformats-officedocument.wordprocessingml.document');
  });

  it('should return "application/vnd.ms-msword"', async () => {
    const stream = fs.createReadStream(path.join(__dirname, 'resources/document.doc'));
    const reader = new FiletypeReader(stream);
    const fileInfo = await reader.read();
    assert.equal(fileInfo.ext, 'doc');
    assert.equal(fileInfo.mime, 'application/vnd.ms-word');
  });

  it('should return "application/vnd.oasis.opendocument.text"', async () => {
    const stream = fs.createReadStream(path.join(__dirname, 'resources/document.odt'));
    const reader = new FiletypeReader(stream);
    const fileInfo = await reader.read();
    assert.equal(fileInfo.ext, 'odt');
    assert.equal(fileInfo.mime, 'application/vnd.oasis.opendocument.text');
  });

  it('should return "text/rtf"', async () => {
    const stream = fs.createReadStream(path.join(__dirname, 'resources/document.rtf'));
    const reader = new FiletypeReader(stream);
    const fileInfo = await reader.read();
    assert.equal(fileInfo.ext.includes('rtf'), true);
    assert.equal(fileInfo.mime, 'text/rtf');
  });
  /// endregion

  /// region Spreadsheet
  it('should return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"', async () => {
    const stream = fs.createReadStream(path.join(__dirname, 'resources/spreadsheet.xlsx'));
    const reader = new FiletypeReader(stream);
    const fileInfo = await reader.read();
    assert.equal(fileInfo.ext, 'xlsx');
    assert.equal(fileInfo.mime, 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
  });

  it('should return "application/vnd.ms-excel"', async () => {
    const stream = fs.createReadStream(path.join(__dirname, 'resources/spreadsheet.xls'));
    const reader = new FiletypeReader(stream);
    const fileInfo = await reader.read();
    assert.equal(fileInfo.ext, 'xls');
    assert.equal(fileInfo.mime, 'application/vnd.ms-excel');
  });

  it('should return "application/vnd.oasis.opendocument.spreadsheet"', async () => {
    const stream = fs.createReadStream(path.join(__dirname, 'resources/spreadsheet.ods'));
    const reader = new FiletypeReader(stream);
    const fileInfo = await reader.read();
    assert.equal(fileInfo.ext, 'ods');
    assert.equal(fileInfo.mime, 'application/vnd.oasis.opendocument.spreadsheet');
  });
  /// endregion

  /// region Presentation
  it('should return "application/vnd.openxmlformats-officedocument.presentationml.presentation"', async () => {
    const stream = fs.createReadStream(path.join(__dirname, 'resources/presentation.pptx'));
    const reader = new FiletypeReader(stream);
    const fileInfo = await reader.read();
    assert.equal(fileInfo.ext, 'pptx');
    assert.equal(fileInfo.mime, 'application/vnd.openxmlformats-officedocument.presentationml.presentation');
  });

  it('should return "application/vnd.ms-powerpoint"', async () => {
    const stream = fs.createReadStream(path.join(__dirname, 'resources/presentation.ppt'));
    const reader = new FiletypeReader(stream);
    const fileInfo = await reader.read();
    assert.equal(fileInfo.ext, 'ppt');
    assert.equal(fileInfo.mime, 'application/vnd.ms-powerpoint');
  });

  it('should return "application/vnd.oasis.opendocument.presentation"', async () => {
    const stream = fs.createReadStream(path.join(__dirname, 'resources/presentation.odp'));
    const reader = new FiletypeReader(stream);
    const fileInfo = await reader.read();
    assert.equal(fileInfo.ext, 'odp');
    assert.equal(fileInfo.mime, 'application/vnd.oasis.opendocument.presentation');
  });
  /// endregion

  it('should return null for text file', async () => {
    const stream = fs.createReadStream(path.join(__dirname, 'resources/sample.txt'));
    const reader = new FiletypeReader(stream);
    const fileInfo = await reader.read();
    assert.equal(fileInfo, null);
  });

  it('should throw error on constructor if missing source', async () => {
    let error = null;
    try {
      const reader = new FiletypeReader();
    }
    catch (err) {
      error = err;
    }

    assert.isNotNull(error);
  });
});
