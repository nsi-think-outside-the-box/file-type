'use strict';

/**@type {Object<string, {mime: string, ext?: string | string[]}>} */
const MimeTypes = {
    // application/msword
    // application/doc
    // application/vnd.msword
    // application/vnd.ms-word
    // application/winword
    // application/word
    // application/x-msw6
    // application/x-msword
    Doc: {mime: 'application/vnd.ms-word', ext: 'doc'},
    Docx: {mime: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', ext: 'docx'},
    Eml: {mime: 'message/rfc822', ext: 'eml'},
    Epub: {mime: 'application/epub+zip', ext: 'epub'},
    Gif: {mime: 'image/gif', ext: 'gif'},
    Jpeg: {mime: 'image/jpeg', ext: ['jpeg', 'jpg']},
    Msg: {mime: 'application/vnd.ms-outlook', ext: 'msg'},
    Odp: {mime: 'application/vnd.oasis.opendocument.presentation', ext: 'odp'},
    Ods: {mime: 'application/vnd.oasis.opendocument.spreadsheet', ext: 'ods'},
    Odt: {mime: 'application/vnd.oasis.opendocument.text', ext: 'odt'},
    Pdf: {mime: 'application/pdf', ext: 'pdf'},
    Png: {mime: 'image/png', ext: 'png'},
    // application/mspowerpoint
    // application/ms-powerpoint
    // application/powerpoint
    // application/x-powerpoint
    // application/mspowerpnt
    // application/vnd-mspowerpoint
    // application/vnd.ms-powerpoint
    // application/x-mspowerpoint
    Ppt: {mime: 'application/vnd.ms-powerpoint', ext: 'ppt'},
    Pptx: {mime: 'application/vnd.openxmlformats-officedocument.presentationml.presentation', ext: 'pptx'},
    Rtf: {mime: 'text/rtf', ext: ['rtf', 'ztf']},
    // application/excel
    // application/vnd.ms-excel
    // application/msexcell
    // application/x-msexcel
    // application/x-excel
    // application/x-msexcel
    // application/x-dos_ms_excel
    // application/xls
    Xls: {mime: 'application/vnd.ms-excel', ext: 'xls'},
    Xlsx: {mime: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', ext: 'xlsx'},
    Zip: {mime: 'application/zip', ext: 'zip'},
};

module.exports = {MimeTypes};
