'use strict';

module.exports = defaultParser;

const {check, checkString} = require('./util');
const MimeTypes = require('./common-types').MimeTypes;

// doc, docx, xlsx, xls, pdf, png, jpeg, jpg, zip, p7m

/**@param {FiletypeReader} reader
 * @returns {FileType | null} returns null if file type is not recognized */
async function defaultParser(reader) {
    const firstBytes = await reader.readBytes(0, 1024);

    // images
    if (check([0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A], firstBytes)) {
        return MimeTypes.Png;
    }

    // only 'image/jpeg' is a correct mimetype, 'image/jpg' is not
    // same of files with extension 'jpg' and 'jpeg'
    if (check([0xff, 0xd8, 0xff, 0xe0], firstBytes)) {
        return MimeTypes.Jpeg; // jpeg image
    }

    if (check([0xff, 0xd8, 0xff, 0xe1], firstBytes)) {
        return MimeTypes.Jpeg;// exif
    }

    if (check([0xff, 0xd8, 0xff, 0xe8], firstBytes)) {
        return MimeTypes.Jpeg;// spiff
    }

    if (check([0xff, 0xd8, 0xff, 0xe2], firstBytes) ||
        check([0xff, 0xd8, 0xff, 0xe3], firstBytes)) {
        return MimeTypes.Jpeg;
    }

    if (check([0x47, 0x49, 0x46, 0x38], firstBytes)) {
        return MimeTypes.Gif;
    }

    const emlMagicNumbers = [
        [0x78, 0x2D, 0x73, 0x74, 0x6F, 0x72, 0x65, 0x2D, 0x69, 0x6E, 0x66, 0x6F, 0x3A],
        [0x58, 0x2D],
        [0x54, 0x6F, 0x3A, 0x20],
        [0x52, 0x65, 0x74, 0x75, 0x72, 0x6E, 0x2D, 0x52, 0x65, 0x63, 0x65, 0x69, 0x70, 0x74, 0x2D, 0x54],
        [0x52, 0x65, 0x74, 0x75, 0x72, 0x6E, 0x2D, 0x50],
        [0x52, 0x65, 0x74, 0x75, 0x72, 0x6E, 0x2D],
        [0x52, 0x65, 0x63, 0x65, 0x69, 0x76, 0x65, 0x64, 0x3A, 0x20],
        [0x4D, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x2D, 0x49, 0x44, 0x3A, 0x20, 0x3C],
        [0x4D],
        [0x46, 0x72, 0x6F, 0x6D],
        [0x44, 0x65, 0x6C, 0x69, 0x76, 0x65, 0x72, 0x65, 0x64, 0x2D, 0x54, 0x6F, 0x3A, 0x20],
        [0x44, 0x61, 0x74, 0x65, 0x3A, 0x20],
        [0x43, 0x6F, 0x6E, 0x74, 0x65, 0x6E, 0x74, 0x2D, 0x54, 0x79, 0x70, 0x65, 0x3A, 0x20, 0x6D, 0x75]
    ];
    for (const magicNumber of emlMagicNumbers) {
        if (check(magicNumber, firstBytes)) {
            return MimeTypes.Eml;
        }
    }


    if (checkString('%PDF', firstBytes)) {
        return MimeTypes.Pdf;
    }

    if (checkString('{\\rtf1', firstBytes)) {
        return MimeTypes.Rtf;
    }

    if (check([0xD0, 0xCF, 0x11, 0xE0, 0xA1, 0xB1, 0x1A, 0xE1], firstBytes)) {
        const xlsMagicNumbers = [
            [0x09, 0x08, 0x10, 0x00, 0x00, 0x06, 0x05, 0x00],
            [0xfd, 0xff, 0xff, 0xff, 0x10],
            [0xfd, 0xff, 0xff, 0xff, 0x1f],
            [0xfd, 0xff, 0xff, 0xff, 0x22],
            [0xfd, 0xff, 0xff, 0xff, 0x23],
            [0xfd, 0xff, 0xff, 0xff, 0x28],
            [0xfd, 0xff, 0xff, 0xff, 0x29]
        ];
        for (const magicNumber of xlsMagicNumbers) {
            if (check(magicNumber, firstBytes, {offset: 512})) {
                return MimeTypes.Xls;
            }
        }

        const pptMagicNumbersWithMasks = [
            [[0x00, 0x6E, 0x1E, 0xF0], null],
            [[0x0F, 0x00, 0xE8, 0x03], null],
            [[0xA0, 0x46, 0x1D, 0xF0], null],
            [[0xFD, 0xFF, 0xFF, 0xFF, 0x0E, 0x00, 0x00, 0x00], null],
            [[0xFD, 0xFF, 0xFF, 0xFF, 0x1C, 0x00, 0x00, 0x00], null],
            [[0xFD, 0xFF, 0xFF, 0xFF, 0x43, 0x00, 0x00, 0x00], null],
            // in alcuni siti danno i byte 6 a 7 come "nn" che immagino stia per "qualsiasi cosa" quindi uso maschera
            [[0xFD, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00], [0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0xFF, 0xFF]]
        ];
        for (const magicNumber of pptMagicNumbersWithMasks) {
            if (check(magicNumber[0], firstBytes, {offset: 512, mask: magicNumber[1]})) {
                return MimeTypes.Ppt;
            }
        }

        //return MimeTypes.Doc;
        const docMagicNumberWithOffset = [0xec, 0xa5, 0xc1, 0x00];
        if (check(docMagicNumberWithOffset, firstBytes, {offset: 512})) {
            return MimeTypes.Doc;
        }
    }

    const docMagicNumbers = [
        [0xd0, 0x44, 0x4f, 0x43],
        [0xcf, 0x11, 0xe0, 0xa1, 0xb1, 0x1a, 0xe1, 0x00],
        [0xdb, 0xa5, 0x2d, 0x00]
    ];
    for (const magicNumber of docMagicNumbers) {
        if (check(magicNumber, firstBytes)) {
            return MimeTypes.Doc;
        }
    }

    const pptMagicNumbers = [
        [0xED, 0xDE, 0xAD, 0x0B, 0x02, 0x00, 0x00, 0x00],
        [0xED, 0xDE, 0xAD, 0x0B, 0x0, 0x00, 0x00, 0x00],
    ];
    for (const magicNumber of pptMagicNumbers) {
        if (check(magicNumber, firstBytes)) {
            return MimeTypes.Ppt;
        }
    }

    // deve stare dopo
    if (check([0xD0, 0xCF, 0x11, 0xE0, 0xA1, 0xB1, 0x1A, 0xE1, 0x00], firstBytes)) {
        return MimeTypes.Msg;
    }

    // zip based file formats
    if (check([0x50, 0x4b, 0x03, 0x04], firstBytes)) {
        const ZIP_HEADER_SIZE = 30;
        let current = 0;
        let headerBuffer = firstBytes;
        while (current + ZIP_HEADER_SIZE < reader.buffer.length) {
            const compressedSize = headerBuffer.readUInt32LE(18);
            const uncompressedSize = headerBuffer.readUInt32LE(22);
            const filenameLength = headerBuffer.readUInt16LE(26);
            const extraFieldLength = headerBuffer.readInt16LE(28);

            current += ZIP_HEADER_SIZE;
            const filename = (await reader.readBytes(current, filenameLength)).toString();
            current += filename.length + extraFieldLength;

            if (filename === 'META-INF/mozilla.rsa') {
                return {ext: 'xpi', mime: 'application/x-xpinstall'};
            }

            if (filename.endsWith('.rels')) {
                const type = filename.split('/')[0];
                switch (type) {
                    case '_rels':
                        break;
                    case 'word':
                        return MimeTypes.Docx;
                    case 'ppt':
                        return MimeTypes.Pptx;
                    case 'xl':
                        return MimeTypes.Xlsx;
                    default:
                        break;
                    //return;
                }
            }

            if (filename.startsWith('xl/')) {
                return MimeTypes.Xlsx;
            }

            if (filename === 'mimetype' && compressedSize === uncompressedSize) {
                const mimeType = (await reader.readBytes(current, uncompressedSize)).toString();
                current += mimeType.length;

                switch (mimeType) {
                    case 'application/epub+zip':
                        return MimeTypes.Epub;
                    case 'application/vnd.oasis.opendocument.text':
                        return MimeTypes.Odt;
                    case 'application/vnd.oasis.opendocument.spreadsheet':
                        return MimeTypes.Ods;
                    case 'application/vnd.oasis.opendocument.presentation':
                        return MimeTypes.Odp;
                    default:
                        break;
                }
            } else {
                current += compressedSize;
            }

            headerBuffer = await reader.readBytes(current, ZIP_HEADER_SIZE);
        }

        return MimeTypes.Zip;
    }

    return null;
}
