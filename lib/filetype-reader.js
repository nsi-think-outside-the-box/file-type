'use strict';
const Readable = require('stream').Readable;
const defaultParser = require('./default-parser');

// List of mime types: https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types

/**@typedef {object} FileType
 * @property {string} mime
 * @property {string} ext*/

/**@typedef {function} filetypeParser
 * @param {FiletypeReader} reader
 * @returns {Promise<FileType>}*/

class FiletypeReader {
  /**
   * source deve essere un Buffer o ReadableStream
   * parsers(opzionale) è un array di funzioni(async)
   *   che prendono come parametro questo oggetto e devono leggere il tipo di file usando readBytes e buffer,
   *   il parser default supporta: 'png', 'jpeg', 'jpg', 'pdf', 'docx', 'zip', 'xlsx', 'xls'
   *
   * @param {Buffer | ReadableStream} source
   * @param {Array<filetypeParser>} [parsers] */
  constructor(source, parsers) {
    if (!source) {
      throw new Error('source cannot be null');
    }

    if (source instanceof Buffer) {
      this._buffer = source;
    }
    else if (source instanceof Readable) {
      this._buffer = null;
      this._stream = source;
    }
    else {
      throw new Error('source must be a Buffer or ReadableStream');
    }

    this._parsers = parsers && parsers.length ? parsers : [defaultParser];
  }

  /**@returns {Promise<FileType>} */
  async read() {
    if (this._stream) {
      await new Promise((resolve, reject) => {
        const chunks = [];
        this._stream.on('data', data => {
          chunks.push(data);
        });
        this._stream.on('end', () => {
          this._buffer = Buffer.concat(chunks);
          resolve();
        });
        this._stream.on('error', err => reject(err));
      });
    }

    for (const parser of this._parsers) {
      const result = await parser(this);
      if (result) {
        return result;
      }
    }
    return null;
  }

  /**@returns {Buffer} */
  get buffer() {
    return this._buffer;
  }

  /**@param {number} offset
   * @param {number} length
   * @returns {Promise<Buffer>} */
  async readBytes(offset, length) {
    return this._buffer.slice(offset, offset + length);
  }
}

module.exports = FiletypeReader;
