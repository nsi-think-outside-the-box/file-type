'use strict';

/**@returns {boolean} */
function check(/**Buffer | number[]*/testBuffer, /**Buffer*/buffer, {/**number*/offset, /**Buffer*/mask} = {}) {
  offset = offset || 0;

  for (let i = 0; i < testBuffer.length; i++) {
    if (mask && testBuffer[i] !== (buffer[i + offset] & mask[i])) {
      return false;
    }
    if (!mask && testBuffer[i] !== buffer[i + offset]) {
      return false;
    }
  }
  return true;
}

function checkString(/**string*/testStr, buffer, {/**number*/offset, /**Buffer*/mask} = {}) {
  offset = offset || 0;

  for (let i = 0; i < testStr.length; i++) {
    if (mask && testStr.charCodeAt(i) !== (buffer[i + offset] & mask[i])) {
      return false;
    }
    if (!mask && testStr.charCodeAt(i) !== buffer[i + offset]) {
      return false;
    }
  }
  return true;
}

module.exports = {check, checkString};
